package com.github.PruneRpc.registry;


public interface ServiceDiscovery {

    String discover(String serviceName);
}