package com.github.PruneRpc.registry;

public interface ServiceRegistry {

    void register(String serviceName, String serviceAddress);
}